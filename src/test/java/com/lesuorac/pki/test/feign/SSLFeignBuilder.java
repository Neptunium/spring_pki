package com.lesuorac.pki.test.feign;

import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.commons.io.FileUtils;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;

import feign.Client;
import feign.Feign;

public class SSLFeignBuilder {
	public static Feign.Builder createSSLFeignBuilder(String clientp12, String clientPassword, String caCrt)
			throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException,
			UnrecoverableKeyException, KeyManagementException {
		KeyStore clientKeyStore = KeyStore.getInstance("PKCS12");
		clientKeyStore.load(FileUtils.openInputStream(new File(clientp12)), clientPassword.toCharArray());

		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		kmf.init(clientKeyStore, clientPassword.toCharArray());

		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		java.security.cert.Certificate caCert = cf.generateCertificate(FileUtils.openInputStream(new File(caCrt)));

		KeyStore CAKeyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		CAKeyStore.load(null, null);
		CAKeyStore.setCertificateEntry("CA", caCert);

		String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
		tmf.init(CAKeyStore);

		SSLContext sslContext = SSLContext.getInstance("TLS");
		sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

		return Feign.builder().logLevel(feign.Logger.Level.FULL)
				.client(new Client.Default(sslContext.getSocketFactory(),
						SSLConnectionSocketFactory.getDefaultHostnameVerifier()))
				.contract(new SpringMvcContract());
	}
}
