package com.lesuorac.pki.test.feign;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import feign.Feign;

@Configuration
public class User4FeignConfiguration {

	@Bean
	@Scope("prototype")
	public Feign.Builder feignBuilder() {

		try {
			return SSLFeignBuilder.createSSLFeignBuilder(
					"/ssd/workspace/spring_pki/src/main/resources/certs/client4.p12", "password",
					"/ssd/workspace/spring_pki/src/main/resources/certs/ca.crt");
		} catch (Exception e) {
			e.printStackTrace();
			throw new IllegalArgumentException(e);
		}
	}

}
