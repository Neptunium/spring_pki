package com.lesuorac.pki.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.lesuorac.pki.test.feign.User1FeignConfiguration;
import com.lesuorac.pki.test.feign.User2FeignConfiguration;
import com.lesuorac.pki.test.feign.User3FeignConfiguration;
import com.lesuorac.pki.test.feign.User4FeignConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PkiTestApplication.class)
@WebIntegrationTest
public class PkiUserCertTests {

	@FeignClient(name = "User1", url = "https://localhost:8443", configuration = User1FeignConfiguration.class)
	interface User1FeignClient extends PkiTestFeignInterface {

	}

	@FeignClient(name = "User2", url = "https://localhost:8443", configuration = User2FeignConfiguration.class)
	interface User2FeignClient extends PkiTestFeignInterface {

	}

	@FeignClient(name = "User3", url = "https://localhost:8443", configuration = User3FeignConfiguration.class)
	interface User3FeignClient extends PkiTestFeignInterface {

	}

	@FeignClient(name = "User4", url = "https://localhost:8443", configuration = User4FeignConfiguration.class)
	interface User4FeignClient extends PkiTestFeignInterface {

	}

	@Autowired
	User1FeignClient user1FeignClient;

	@Autowired
	User2FeignClient user2FeignClient;

	@Autowired
	User3FeignClient user3FeignClient;

	@Autowired
	User4FeignClient user4FeignClient;

	@Test
	public void contextLoads() {
	}

	@Test
	public void testclientcert_client1() {
		Assert.assertEquals("user1", user1FeignClient.getUsername());
	}
	
	@Test
	public void testclientcert_client2() {
		Assert.assertEquals("user2", user2FeignClient.getUsername());
	}
	
	@Test
	public void testclientcert_client3() {
		Assert.assertEquals("user3", user3FeignClient.getUsername());
	}
	
	@Test
	public void testclientcert_client4() {
		Assert.assertEquals("user4", user4FeignClient.getUsername());
	}

}
