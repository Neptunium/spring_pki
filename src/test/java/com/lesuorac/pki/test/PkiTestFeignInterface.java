package com.lesuorac.pki.test;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface PkiTestFeignInterface {

	@RequestMapping(path = "/getUser", method = RequestMethod.GET)
	String getUsername();

	@RequestMapping(path = "/", method = RequestMethod.GET)
	String getRoot();

}
