#!/bin/bash
 
# default password for keys
PASSWORD=password
 
OUT_DIR=certs
 
# Subject items
C="US"
ST="My State"
L="My City"
O="My Corp"
 
CN_CA="_PKI Test CA"
CN_SERVER="localhost"

CN_CLIENT1="user1"
CN_CLIENT2="user2"
CN_CLIENT3="user3"
CN_CLIENT4="user4"
 
###############################
 
# Create output directory
rm -rf ${OUT_DIR}
mkdir -p ${OUT_DIR}
 
###############################
 
# create CA key
openssl genrsa -des3 -out ${OUT_DIR}/ca.key -passout pass:$PASSWORD 4096
 
# create CA cert
openssl req -new -x509 -days 365 -key ${OUT_DIR}/ca.key -out ${OUT_DIR}/ca.crt \
 -passin pass:$PASSWORD -subj "/C=${C}/ST=${ST}/L=${L}/O=${O}/CN=${CN_CA}/"
 
# create truststore
keytool -import -trustcacerts -alias caroot -file ${OUT_DIR}/ca.crt \
 -keystore ${OUT_DIR}/truststore.jks -storepass ${PASSWORD} -noprompt
 
###############################
 
# create server key
openssl genrsa -des3 -out ${OUT_DIR}/server.key -passout pass:$PASSWORD 4096
 
# create server cert request
openssl req -new -key ${OUT_DIR}/server.key -out ${OUT_DIR}/server.csr \
 -passin pass:$PASSWORD -subj "/C=${C}/ST=${ST}/L=${L}/O=${O}/CN=${CN_SERVER}/"
 
# create server cert
openssl x509 -req -days 365 -in ${OUT_DIR}/server.csr -CA ${OUT_DIR}/ca.crt \
 -CAkey ${OUT_DIR}/ca.key -set_serial 01 -out ${OUT_DIR}/server.crt \
 -passin pass:${PASSWORD}
 
# convert server cert to PKCS12 format, including key
openssl pkcs12 -export -out ${OUT_DIR}/server.p12 -inkey ${OUT_DIR}/server.key \
 -in ${OUT_DIR}/server.crt -passin pass:${PASSWORD} -passout pass:${PASSWORD}
 
###############################
 
## Client 1
# create client key
openssl genrsa -des3 -out ${OUT_DIR}/client1.key -passout pass:${PASSWORD} 4096
 
# create client cert request
openssl req -new -key ${OUT_DIR}/client1.key -out ${OUT_DIR}/client1.csr \
 -passin pass:$PASSWORD -subj "/C=${C}/ST=${ST}/L=${L}/O=${O}/CN=${CN_CLIENT1}/"
 
 
# create client cert
openssl x509 -req -days 365 -in ${OUT_DIR}/client1.csr -CA ${OUT_DIR}/ca.crt \
 -CAkey ${OUT_DIR}/ca.key -set_serial 02 -out ${OUT_DIR}/client1.crt \
 -passin pass:${PASSWORD}
 
# convert client cert to PKCS12, including key
openssl pkcs12 -export -out ${OUT_DIR}/client1.p12 -inkey ${OUT_DIR}/client1.key \
 -in ${OUT_DIR}/client1.crt -passin pass:${PASSWORD} -passout pass:${PASSWORD}
 
 
 ## Client 2
 # create client key
openssl genrsa -des3 -out ${OUT_DIR}/client2.key -passout pass:${PASSWORD} 4096
 
# create client cert request
openssl req -new -key ${OUT_DIR}/client2.key -out ${OUT_DIR}/client2.csr \
 -passin pass:$PASSWORD -subj "/C=${C}/ST=${ST}/L=${L}/O=${O}/CN=${CN_CLIENT2}/"
 
 
# create client cert
openssl x509 -req -days 365 -in ${OUT_DIR}/client2.csr -CA ${OUT_DIR}/ca.crt \
 -CAkey ${OUT_DIR}/ca.key -set_serial 03 -out ${OUT_DIR}/client2.crt \
 -passin pass:${PASSWORD}
 
# convert client cert to PKCS12, including key
openssl pkcs12 -export -out ${OUT_DIR}/client2.p12 -inkey ${OUT_DIR}/client2.key \
 -in ${OUT_DIR}/client2.crt -passin pass:${PASSWORD} -passout pass:${PASSWORD}
 
## Client 3
 # create client key
openssl genrsa -des3 -out ${OUT_DIR}/client3.key -passout pass:${PASSWORD} 4096
 
# create client cert request
openssl req -new -key ${OUT_DIR}/client3.key -out ${OUT_DIR}/client3.csr \
 -passin pass:$PASSWORD -subj "/C=${C}/ST=${ST}/L=${L}/O=${O}/CN=${CN_CLIENT3}/"
 
 
# create client cert
openssl x509 -req -days 365 -in ${OUT_DIR}/client3.csr -CA ${OUT_DIR}/ca.crt \
 -CAkey ${OUT_DIR}/ca.key -set_serial 04 -out ${OUT_DIR}/client3.crt \
 -passin pass:${PASSWORD}
 
# convert client cert to PKCS12, including key
openssl pkcs12 -export -out ${OUT_DIR}/client3.p12 -inkey ${OUT_DIR}/client3.key \
 -in ${OUT_DIR}/client3.crt -passin pass:${PASSWORD} -passout pass:${PASSWORD}
 
 
## Client 4
 # create client key
openssl genrsa -des3 -out ${OUT_DIR}/client4.key -passout pass:${PASSWORD} 4096
 
# create client cert request
openssl req -new -key ${OUT_DIR}/client4.key -out ${OUT_DIR}/client4.csr \
 -passin pass:$PASSWORD -subj "/C=${C}/ST=${ST}/L=${L}/O=${O}/CN=${CN_CLIENT4}/"
 
 
# create client cert
openssl x509 -req -days 365 -in ${OUT_DIR}/client4.csr -CA ${OUT_DIR}/ca.crt \
 -CAkey ${OUT_DIR}/ca.key -set_serial 05 -out ${OUT_DIR}/client4.crt \
 -passin pass:${PASSWORD}
 
# convert client cert to PKCS12, including key
openssl pkcs12 -export -out ${OUT_DIR}/client4.p12 -inkey ${OUT_DIR}/client4.key \
 -in ${OUT_DIR}/client4.crt -passin pass:${PASSWORD} -passout pass:${PASSWORD}
