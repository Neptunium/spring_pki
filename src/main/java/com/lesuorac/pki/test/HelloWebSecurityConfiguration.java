package com.lesuorac.pki.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class HelloWebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		// http://ingini.org/2015/03/26/authentication-authorization-schema-design-with-mongodb/
		auth.inMemoryAuthentication()//
				.withUser("user1").password("password").roles("USER")//
				.and().withUser("user2").password("password").roles("USER")//
				.and().withUser("user3").password("password").roles("USER")//
				.and().withUser("user4").password("password").roles("USER");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()//
				.anyRequest()//
				.hasRole("USER")//
				.and()//
				.x509();
	}
}