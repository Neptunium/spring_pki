package com.lesuorac.pki.test;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableFeignClients
public class PkiTestApplication {

	private static final Logger LOGGER = LogManager.getFormatterLogger();

	public static void main(String[] args) {
		SpringApplication.run(PkiTestApplication.class, args);
	}

	@RequestMapping(path = "/getUser", method = RequestMethod.GET)
	public String getRandomString(HttpServletRequest request, Principal principal) {
		LOGGER.info(
				"User Principal:[%s] Request:[%s] made request from remote [%s@%s:%d] to local [%s@%s:%d] for /getUser",
				principal.getName(), request.getRemoteUser(), request.getRemoteHost(), request.getRemoteAddr(),
				request.getRemotePort(), request.getLocalName(), request.getLocalAddr(), request.getLocalPort());
		return request.getRemoteUser();
	}
}
